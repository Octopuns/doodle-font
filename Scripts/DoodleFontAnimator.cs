﻿namespace DoodleStudio95.Extensions.Fonts
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.UI;

	/// <summary>
	/// Automatically set up the Text component when the DoodleFont is changed
	/// </summary>
	public class DoodleFontAnimator : MonoBehaviour
	{
		public DoodleFont doodleFont;

		public void OnPreRender()
		{
			Text text = GetComponent<Text>();
			text.font = doodleFont.customFont;
			text.material = doodleFont.animatedMaterial;
		}
		public void OnValidate()
		{
			if (doodleFont == null)
				return;

			Text text = GetComponent<Text>();
			text.font = doodleFont.customFont;
			text.material = doodleFont.animatedMaterial;
		}
	}
}