﻿namespace DoodleStudio95.Extensions.Fonts
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEditor;
	using System.IO;
	using System.Xml;
	using System;

	/// <summary>
	/// CustomInspector for DoodleFont allowing auto-generation of required assets
	/// from a BitmapFont texture.
	/// </summary>
	[CustomEditor(typeof(DoodleFont))]
	public class DoodleFontCustomInspector : Editor
	{
		#region Public API

		public override void OnInspectorGUI()
		{
	
			DoodleFont doodleFont = target as DoodleFont;

			//get doodle font path and directory
			string doodleFontPath = AssetDatabase.GetAssetPath(doodleFont);
			string doodleFontDirectory = Path.GetDirectoryName(doodleFontPath);
			string doodleFontDataDirectory = doodleFontDirectory + "/data/";

			if(Directory.Exists(doodleFontDataDirectory) == false)
				Directory.CreateDirectory(doodleFontDataDirectory);



			EditorGUILayout.LabelField("Bitmap Font", EditorStyles.centeredGreyMiniLabel);
			GUILayout.Space(10.0f);

			EditorGUILayout.LabelField("Attach a bitmap font texture and click Generate to create necessary assets and a DoodleAnimationFile.", EditorStyles.wordWrappedLabel);

			doodleFont.bitmapFont = EditorGUILayout.ObjectField("Bitmap Font", doodleFont.bitmapFont, typeof(Texture2D), false ) as Texture2D;
			doodleFont.bitmapData = EditorGUILayout.ObjectField("Bitmap Data", doodleFont.bitmapData, typeof(TextAsset), false) as TextAsset;

			GUILayout.Space(5.0f);

			if (doodleFont.bitmapFont != null && doodleFont.bitmapData != null && GUILayout.Button("Generate Doodle Font Data"))
			{
				Debug.Log("Generate the doodle font information");

				string customFontPath = doodleFontDirectory + "/" + doodleFont.bitmapFont.name + ".asset";
				doodleFont.customFont = CreateCustomFont(customFontPath, doodleFont.bitmapFont, doodleFont.bitmapData);

				string animationFilePath = doodleFontDataDirectory + "/" + doodleFont.name + "-AnimationFile.asset";
				doodleFont.doodleAnimationFile = CreateDoodleAnimationFile(animationFilePath, doodleFont.bitmapFont, out doodleFont.bitmapFontDuplicate);

				//create font material
				string fontMaterialPath = doodleFontDataDirectory + "/" + doodleFont.name + "-FontMaterial.asset";
				doodleFont.customFont.material = CreateMaterial(fontMaterialPath, Shader.Find("Unlit/Texture"));
				doodleFont.customFont.material.mainTexture = doodleFont.bitmapFont;

				//create the animated material
				string animatedMaterialPath = doodleFontDataDirectory + "/" + doodleFont.name + "-AnimatedMaterial.asset";
				doodleFont.animatedMaterial = CreateMaterial(animatedMaterialPath, Shader.Find("Unlit/DoodleFontAnimated"));
				doodleFont.animatedMaterial.mainTexture = doodleFont.bitmapFont;
			
				EditorUtility.SetDirty(doodleFont);
				EditorUtility.SetDirty(doodleFont.customFont);
				EditorUtility.SetDirty(doodleFont.animatedMaterial);
				AssetDatabase.SaveAssets();
			}

			//if the font is not ready, display options to get it ready
			if (IsFontReady(doodleFont))
			{
				GUILayout.Space(30.0f);
				EditorGUILayout.LabelField("Spritesheet Generation", EditorStyles.centeredGreyMiniLabel);
				GUILayout.Space(10.0f);
				EditorGUILayout.LabelField("Draw your font frames inside your Doodle Animation File and then click generate to create a spritesheet", EditorStyles.wordWrappedLabel);

				doodleFont.doodleAnimationFile = EditorGUILayout.ObjectField("Animation File", doodleFont.doodleAnimationFile, typeof(DoodleAnimationFile), false) as DoodleAnimationFile;
	
				EditorGUILayout.ObjectField("SpriteSheet", doodleFont.spriteSheet, typeof(Texture2D), false);

				doodleFont.enforceSquares = EditorGUILayout.Toggle("Force Square", doodleFont.enforceSquares);
				doodleFont.convertToWhite = EditorGUILayout.Toggle("Convert to White", doodleFont.convertToWhite);
				doodleFont.animationSpeed = EditorGUILayout.FloatField("Animation Speed", doodleFont.animationSpeed);

				//otherwise, font is ready
				if (GUILayout.Button("Generate SpriteSheet"))
				{
					//grab all textures from the DoodleAnimationFile
					List<Texture2D> textures = new List<Texture2D>();
					foreach (DoodleAnimationFileKeyframe keyframe in doodleFont.doodleAnimationFile.frames)
						textures.Add(keyframe.Texture);

					//pack all frames into a single spritesheet
					string spriteSheetPath = doodleFontDataDirectory + "/" + doodleFont.name + "-SpriteSheet.png";
					doodleFont.spriteSheet = CreateSpritesheet(spriteSheetPath, textures.ToArray(), doodleFont.enforceSquares, doodleFont.convertToWhite, out doodleFont.rows, out doodleFont.columns);
					doodleFont.frames = textures.Count;

					//set spritesheet as the materials
					doodleFont.animatedMaterial.mainTexture = doodleFont.spriteSheet;
					doodleFont.customFont.material.mainTexture = doodleFont.spriteSheet;

					//set the parameters of the animated material
					doodleFont.animatedMaterial.SetInt("Columns", doodleFont.columns);
					doodleFont.animatedMaterial.SetInt("Rows", doodleFont.rows);
					doodleFont.animatedMaterial.SetInt("Frames", doodleFont.frames);
					doodleFont.animatedMaterial.SetFloat("Speed", doodleFont.animationSpeed);

					EditorUtility.SetDirty(doodleFont.animatedMaterial);
					EditorUtility.SetDirty(doodleFont.customFont);
					EditorUtility.SetDirty(doodleFont.customFont.material);
					EditorUtility.SetDirty(doodleFont);

					AssetDatabase.SaveAssets();
				}

				GUILayout.Space(30.0f);
				EditorGUILayout.LabelField("Delete Font", EditorStyles.centeredGreyMiniLabel);
				GUILayout.Space(10.0f);
				EditorGUILayout.LabelField("Click delete to delete all generated font data and start again. Be careful, this will destroy your doodle animation file", EditorStyles.wordWrappedLabel);

				if (GUILayout.Button("Delete Font Data"))
				{
					DeleteFontAssets(doodleFont, doodleFontDataDirectory);
				}

			}
		}
	

		/// <summary>
		/// Create a new material asset at path with the passed in shader.
		/// </summary>
		public Material CreateMaterial( string path, Shader shader)
		{
			Material newMaterial = new Material(shader);
			return CreateAsset<Material>(newMaterial, path);
		}

		/// <summary>
		/// Create a CustomFont at path based on the passed in Bitmap Font. There must be a .xml, .fnt or .txt with
		/// the same name as the bitmap font inside the same directory as the BitmapFont asset. 
		/// </summary>
		public Font CreateCustomFont(string path, Texture2D bitmapFont, TextAsset bitmapData)
		{
			//create a custom font using doodleFont.bitmapFont
			Font fontMemory = new Font(bitmapFont.name);

			//custom font
			Font customFont = CreateAsset<Font>(fontMemory, path);
		
			//get the directory path of the bitmap font
			string directory = Path.GetDirectoryName( AssetDatabase.GetAssetPath(bitmapFont));

			//check if there's xml, fnt or txt file or other supportted file that have the same name
			string[] fileExtensions = new string[3] { "xml", "fnt", "txt" };

			//parse the font data
			foreach (string ext in fileExtensions)
			{
				if (ParseFontXML(bitmapData, customFont))
				{
					Debug.Log("Font data successfully found: " + target.name + "." + ext);
					return customFont;
				}
			}

			return null;
		}

		/// <summary>
		/// Create a spritesheet at path by packing textures into a single texture. Fills rows and cols
		/// with the amount of rows and columns (of textures) in the resulting texture. If enforceSquares is true, the
		/// spritesheet will be created with the same amount of rows and columns. For example, with enforceSquares set to true
		/// and 3 textures passed in - the resulting spritesheet will have 2 rows and 2 columns (with one blank).
		/// </summary>
		public Texture2D CreateSpritesheet(string path, Texture2D[] textures, bool enforceSquares, bool convertToWhite, out int rows, out int cols)
		{
			rows = 1;
			cols = 1;

			//rows and columns
			Texture2D packedAtlas = PackTextures(textures, enforceSquares, convertToWhite, out rows, out cols);
			packedAtlas.Apply();
			byte[] pngBytes = packedAtlas.EncodeToPNG();

			File.WriteAllBytes(path, pngBytes);
			AssetDatabase.ImportAsset(path);
			Texture2D spriteSheet = AssetDatabase.LoadAssetAtPath(path, typeof(Texture2D)) as Texture2D;

			return spriteSheet;
		}

		/// <summary>
		/// Create DoodleAnimationFile at path with the specified bitmapFontAsset as the first four frames. On success, bitmapFontDuplicate will
		/// contain a duplicate READABLE texture created from bitmapFontAsset.
		/// </summary>
		public DoodleAnimationFile CreateDoodleAnimationFile(string path, Texture2D bitmapFontAsset, out Texture2D bitmapFontDuplicate)
		{
			//get the directory
			string directory = Path.GetDirectoryName(path);

			//get the path of the bitmap font asset
			string bitmapFontPath = AssetDatabase.GetAssetPath(bitmapFontAsset);
			string bitmapFontDirectory = Path.GetDirectoryName(bitmapFontPath);
			//set the name of the duplicate image
			string bitmapFontDuplicatePath = directory + "/" + bitmapFontAsset.name + "-BitmapFontCopy" + Path.GetExtension(bitmapFontPath);

			//copy the bitmap font into the new path
			AssetDatabase.CopyAsset(bitmapFontPath, bitmapFontDuplicatePath);

			//load the duplicate texture from asset database
			Texture2D duplicateTexture = AssetDatabase.LoadAssetAtPath(bitmapFontDuplicatePath, typeof(Texture2D)) as Texture2D;

			TextureImporter textureImporter = AssetImporter.GetAtPath(bitmapFontDuplicatePath) as TextureImporter;
			textureImporter.isReadable = true;
			textureImporter.SaveAndReimport();

			bitmapFontDuplicate = duplicateTexture;

			//create a new animation file and frames list
			DoodleAnimationFile animationFile = ScriptableObject.CreateInstance<DoodleAnimationFile>();
			animationFile.frames = new List<DoodleAnimationFileKeyframe>();

			//add the duplicate texture as a frame in the doodle animation file
			for(int i = 0; i < 4; i++)
				animationFile.frames.Add(new DoodleAnimationFileKeyframe(1, duplicateTexture));

			//load the custom animation file asset
			DoodleAnimationFile customAnimationFile = CreateAsset<DoodleAnimationFile>(animationFile, path);
			return customAnimationFile;
		}

		/// <summary>
		/// Delete all generated font assets stored in the data folder
		/// </summary>
		public void DeleteFontAssets( DoodleFont doodleFont, string doodleFontDataDirectory)
		{
			if (doodleFont.customFont.material != null)
				AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(doodleFont.customFont.material));
			if (doodleFont.customFont != null)
				AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(doodleFont.customFont));
			if (doodleFont.bitmapFontDuplicate != null)
				AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(doodleFont.bitmapFontDuplicate));
			if (doodleFont.doodleAnimationFile != null)
				AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(doodleFont.doodleAnimationFile));
			if (doodleFont.spriteSheet != null)
				AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(doodleFont.spriteSheet));
			if (doodleFont.animatedMaterial != null)
				AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(doodleFont.animatedMaterial));

			doodleFont.rows = 0;
			doodleFont.columns = 0;
			doodleFont.frames = 0;

		
			AssetDatabase.DeleteAsset(doodleFontDataDirectory);
			AssetDatabase.Refresh();

		}

		#endregion

		#region Private

		/// <summary>
		/// Create asset at path and return the asset created if successful
		/// </summary>
		private T CreateAsset<T>(T asset, string path) where T : UnityEngine.Object
		{
			AssetDatabase.CreateAsset(asset, path);
			AssetDatabase.Refresh();

			T createdAsset = AssetDatabase.LoadAssetAtPath(path, typeof(T)) as T;
			return createdAsset;
		}

		/// <summary>
		/// Pack the passed in textures into a single texture
		/// </summary>
		private Texture2D PackTextures(Texture2D[] textures, bool enforceSquare, bool convertToWhite, out int rows, out int columns)
		{
			int textureWidth = textures[0].width;
			int textureCount = textures.Length;


			if (enforceSquare)
			{
				//set rows and columns
				rows = (Mathf.CeilToInt (Mathf.Sqrt(textureCount)));
				columns = rows;
			}
			else
			{
				//set rows and columns
				rows = (Mathf.FloorToInt(Mathf.Sqrt(textureCount)));
				columns = rows;
				int spots = rows * columns;

				if (textureCount % spots == 0)
				{
					columns *= textureCount / spots;
				}
				else
				{
					columns += Mathf.CeilToInt((textureCount - spots) / (float)rows);
				}
			}


			

			//now we know the desired rows and columns
			Texture2D packedTexture = new Texture2D(columns * textureWidth, rows * textureWidth);

			for (int t = 0; t < textures.Length; t++)
			{
				//copy each texture in
				int row = Mathf.FloorToInt(t / columns);
				int col = t - row * columns;

				int pixelOffsetX = col * textureWidth;
				int pixelOffsetY = row * textureWidth;

				int packedTexturePixelCount = packedTexture.GetPixels().Length;

				//for each pixel in the smaller textures
				for(int x = 0; x < textures[t].height; x++)
				{
					for(int y = 0; y < textures[t].width; y++)
					{
						int pixelX = pixelOffsetX + x;
						int pixelY = pixelOffsetY + y;
						Color texturePixel = textures[t].GetPixel(x, y);

						if(convertToWhite)
						{
							texturePixel.r = 1.0f;
							texturePixel.g = 1.0f;
							texturePixel.b = 1.0f;
						}

						packedTexture.SetPixel(pixelX, pixelY, texturePixel);
					}
				}
			}


			return packedTexture;
		}

		private bool IsFontReady( DoodleFont target )
		{
			if (target.customFont == null)
				return false;

			if (target.doodleAnimationFile == null)
				return false;

			return true;
		}

		/// Adaptation of CustomFontBuilder from jackisgames@gmail.com
		private bool ParseFontXML(TextAsset bitmapData, Font customFont)
		{
			
			TextAsset xmlSource = bitmapData;
			XmlDocument xmlDoc = new XmlDocument();
			try
			{
				xmlDoc.LoadXml(xmlSource.text);
			}
			catch (Exception exception)
			{
				Debug.Log("file is not xml " + exception.Message + " " + bitmapData.name);
				return false;
			}
			finally
			{
				XmlNode info = xmlDoc.GetElementsByTagName("info")[0];
				XmlNode common = xmlDoc.GetElementsByTagName("common")[0];
				XmlNodeList charNodes = xmlDoc.GetElementsByTagName("char");
				int charList = charNodes.Count;
				float width = int.Parse(common.Attributes["scaleW"].Value);
				float height = int.Parse(common.Attributes["scaleH"].Value);
				Font targetFont = customFont;
				SerializedObject mFont = new SerializedObject(targetFont);
				mFont.FindProperty("m_FontSize").floatValue = float.Parse(info.Attributes["size"].Value);
				mFont.FindProperty("m_LineSpacing").floatValue = float.Parse(common.Attributes["lineHeight"].Value);
				mFont.ApplyModifiedProperties();

				if (charList > 0)
				{
					CharacterInfo[] charInfos = new CharacterInfo[charList];

					for (int i = 0; i < charList; i++)
					{
						XmlNode node = charNodes.Item(i);
						int nodeID = int.Parse(node.Attributes["id"].Value);
						int nodeOffsetX = int.Parse(node.Attributes["xoffset"].Value);
						int nodeOffsetY = int.Parse(node.Attributes["yoffset"].Value);
						int nodeAdvanceX = int.Parse(node.Attributes["xadvance"].Value);
						int nodeX = int.Parse(node.Attributes["x"].Value);
						int nodeY = int.Parse(node.Attributes["y"].Value);
						int nodeWidth = int.Parse(node.Attributes["width"].Value);
						int nodeHeight = int.Parse(node.Attributes["height"].Value);
						CharacterInfo charInfo = new CharacterInfo();
						charInfo.index = nodeID;
						charInfo.uvTopLeft = new Vector2(nodeX / width, 1 - (nodeY) / height);
						charInfo.uvTopRight = new Vector2((nodeX + nodeWidth) / width, 1 - (nodeY) / height);
						charInfo.uvBottomLeft = new Vector2(nodeX / width, 1 - (nodeY + nodeHeight) / height);
						charInfo.uvBottomRight = new Vector2((nodeX + nodeWidth) / width, 1 - (nodeY + nodeHeight) / height);
						charInfo.minX = nodeOffsetX;
						charInfo.maxY = -nodeOffsetY;
						charInfo.maxX = nodeOffsetX + nodeWidth;
						charInfo.minY = -nodeOffsetY - nodeHeight;

						charInfo.advance = nodeAdvanceX;
						charInfos[i] = charInfo;
					}
					targetFont.characterInfo = charInfos;
					EditorUtility.SetDirty(target);

				}
				else
				{
					Debug.Log("the xml is not in font format " + bitmapData.name);
				}
			}
			return true;
		
	
		}


		#endregion
	}
}